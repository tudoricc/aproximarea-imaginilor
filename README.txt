Nume:Vasile Tudor
Grupa:321CC

Tema 1 -Programare genetică pentru aproximarea imaginilor-

Prima parte :
	Am avut de modificat doua functii la prima parte,size si deviation
	(size ast) : m-am folosit de functiile length si flatten la aceasta functie
				flatten imi elimina dintr parantezele-o lista,iar length ma 
				ajuta sa determin lungimea unei liste.
				Am apelat (length (flatten ast)),care imi determina 
				lungimea sirului ast fara paranteze

    (deviation ast): la aceasta functie am mai creeat eu doua functii,prima
					este evalute si a doua calculate 
            
            evaluate : imi evalua o lista in functie de parametrii,
            am folosit let si " ` " aici
			
			calculate: conform enuntului noi trebuia sa facem suma  modulelor 
			diferentelor valorilor ("sumei abaterilor, în modul, de la valorile
			reale.").Daca cele doua liste erau nule atunci returnam zero pentru
			ca nu mai avea rost sa continui ,iar altfel faceam diferenta 
			propriu-zisa

A doua parte:
	In aceasta parte a trebuie sa modificam comportamentul functiilor anterioare
	astfel incat sa nu functioneze doar pe numere ci si pe imagini.De asemenea 
	mentionez ca am adaugat 4 terminali si o functie in plus fata de cei din 
	enunt,motivul pentru care am adaugat terminalii a fost sa vad daca intampina 
	vreo problema algoritmul si daca nu cumva crapa,iar functia adaugata a fost
	doar ca sa fac mai multe operatii cu imagini.Functiile din scheletul de cod
	au forma (nume tip_returnat (parametru1 parametru2)).Generarea de 
	imagini s-a realizat prin modificarea a 4 functii:
		-generate
		-deviation
		-modify-random
		-random-subtree

	(generate depth method):are aceeasi foma ca si cea din symbolic-regression 
		doar ca la aceasta functie am facut sa imi genereze functii random in 
		functie de ce functie procesam in momentul acela.
		La scale ,pot sa modific ambii termeni,la scale dau random[intre 0 si 2]
		pe valoarea de scalare si gasesc o functie random pentru al doilea 
		argument[imaginea],conform formei functiei.
		Functia rotate are acelasi comportament ca si above si am prelucrat-o 
		la fel doar ca singura diferenta era ca valoarea dupa care fac rotatia
		este intre 0 si 360
		Functia above imi permite sa modific ambii parametrii pentru ca ambii 
		sunt imagini si de aceea ii modific pe amandoi

	(deviation ast):conform enuntului eroarea se calcula ca fiind distanta 
		euclidiana in spatiul culorilor("Eroarea de aproximare pentru o imagine
		oarecare în raport cu modelul poate fi calculată ca distanță euclidiană
		în spațiul culorilor: suma pătratelor diferențelor pe cele trei 
		componente cromatice (RGB)").De aceea eu am creat 3 functii auxiliare
		pentru aceasta functie:
			-topower : care imi ridica un numar la o putere,am facut aceasta
			functie prin recursivitate
			-return-lista-imagini :care imi returneaza lista culorilor unei 
			imagini
			-calcul list-ast list-imagine2 : 
				conform linkului [1]: distanta euclidiana se calculeaza ca fiind
				radical din suma diferentelor celor 3 componente pentru fiecare 
				imagine si am facut functia recursiva deoarce doream sa faca
				de mai multe ori si pentru fiecare componenta a celor doua liste
		La functia propriu zisa am apelat calcula de doua liste.Prima lista era
		lista ce continea culorile astului,iar al doilea argument era lista 
		culorilor imaaginii de referinta.
		Pentru ca poate cele 2 imagini nu au aceeasi dimensiune trebuie sa facem
		in asa fel incat sa aiba.Am auzit cum ca ar trebui sa punem imaginea 
		"ast" trebuie pusa pe un chenar de dimensiuni 55 si 55 deoarece aceasta
		era dimensiunea imaginii de referinta si asa am rezolvat problema 
		anterioara.

	(modify-random ast new):modifica un membru al arborelui ast cu un alt arbore
		si de aceea la fel ca la generate am facut in functie de ce functie 
		aveam in momentul acela de prelucrat.
		La functiile scale si rotate nu puteam sa modific doar al doilea
		argument pentru ca functiile pe imagini nu puteau sa imi returneze un 
		numar si de aceea al doilea argument a fost modificat.
		La above puteam sa modific ambii parametrii pentru ca functiile noastre
		returnau imagini si nu strica comportamentul functiei.

	(random-subtree ast):ia un arbore la intamplare din ast.Aici pur si simplu 
		am luat cel de-al 2 lea parametru la fiecare functie ,chair daca eu cred
		ca nu este suta la suta corect,am facut asa.





[1]http://ro.wikipedia.org/wiki/Distan%C8%9Ba_euclidian%C4%83 